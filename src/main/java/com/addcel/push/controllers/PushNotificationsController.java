package com.addcel.push.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.push.entities.ResponseException;
import com.addcel.push.entities.dto.request.RequestDeleteToken;
import com.addcel.push.entities.dto.request.RequestInsertarToken;
import com.addcel.push.entities.dto.request.RequestNotificacion;
import com.addcel.push.entities.dto.response.ResponseEnviarNotificacion;
import com.addcel.push.entities.dto.response.ResponseInsertarToken;
import com.addcel.push.service.PushMessageService;

@RestController
public class PushNotificationsController {
	
	private static final String SAVE_TOKEN = "/saveToken";
	private static final String SEND_MESSAGE = "/sendMessage";
	private static final String REMOVE_TOKEN = "/removeToken";

	@Autowired
	PushMessageService messageService;
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseException> handleException(Exception e) {
        e.printStackTrace();
        int code = -1;
        String message = e.getMessage();
        return new ResponseEntity<>(new ResponseException(code, message), HttpStatus.OK);
    }
	
	@PostMapping(value = SAVE_TOKEN)
	public ResponseEntity<ResponseInsertarToken> guardarToken(@RequestBody RequestInsertarToken request) throws Exception{
		return new ResponseEntity<ResponseInsertarToken>(messageService.insertarTokenUser(request), HttpStatus.OK);
	}
	
	@PostMapping(value = SEND_MESSAGE)
	public ResponseEntity<ResponseEnviarNotificacion> send_message(@RequestBody RequestNotificacion request) throws Exception{
		return new ResponseEntity<ResponseEnviarNotificacion>(messageService.sendNotification(request), HttpStatus.OK);
	}
	
	@DeleteMapping(value = REMOVE_TOKEN)
	public ResponseEntity<ResponseException> deleteToken(@RequestBody RequestDeleteToken request) throws Exception{
		return new ResponseEntity<ResponseException>(messageService.borrarToken(request), HttpStatus.OK);
	}
	
}
