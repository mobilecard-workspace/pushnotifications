package com.addcel.push.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.push.entities.dto.request.MassivePushRequest;
import com.addcel.push.entities.dto.response.MassivePushResponse;
import com.addcel.push.service.MassivePushService;

@RestController
public class MassivePushController {
	
	@Autowired
	private MassivePushService service;
	
	private static final String SEND_MASSIVE = "/sendMassive";

	@PostMapping(value = SEND_MASSIVE)
	public ResponseEntity<MassivePushResponse> sendMassive(@RequestBody MassivePushRequest request) throws Exception{
		return new ResponseEntity<MassivePushResponse>(service.massivePush(request), HttpStatus.OK);
	}
	
}
