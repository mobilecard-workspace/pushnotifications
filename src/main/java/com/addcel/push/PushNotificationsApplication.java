package com.addcel.push;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan("com.addcel.push")
@EntityScan("com.addcel.push.entities")
@EnableJpaRepositories("com.addcel.push.repositories")
@SpringBootApplication
public class PushNotificationsApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder app) {
		return app.sources(PushNotificationsApplication.class);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(PushNotificationsApplication.class, args);
	}

}
