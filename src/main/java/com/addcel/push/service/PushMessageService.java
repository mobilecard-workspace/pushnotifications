package com.addcel.push.service;

import com.addcel.push.entities.ResponseException;
import com.addcel.push.entities.dto.request.RequestDeleteToken;
import com.addcel.push.entities.dto.request.RequestInsertarToken;
import com.addcel.push.entities.dto.request.RequestNotificacion;
import com.addcel.push.entities.dto.response.ResponseEnviarNotificacion;
import com.addcel.push.entities.dto.response.ResponseInsertarToken;

public interface PushMessageService {

	ResponseEnviarNotificacion sendNotification(RequestNotificacion request) throws Exception;
	
	ResponseInsertarToken insertarTokenUser(RequestInsertarToken request) throws Exception;
	
	ResponseException borrarToken(RequestDeleteToken request) throws Exception;
	
}
