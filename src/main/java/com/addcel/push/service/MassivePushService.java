package com.addcel.push.service;

import com.addcel.push.entities.dto.request.MassivePushRequest;
import com.addcel.push.entities.dto.response.MassivePushResponse;

public interface MassivePushService {

	MassivePushResponse massivePush(MassivePushRequest request) throws Exception;
}
