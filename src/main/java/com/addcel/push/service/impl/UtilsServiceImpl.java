package com.addcel.push.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.push.entities.BitacoraPush;
import com.addcel.push.repositories.BitacoraPushRepository;
import com.addcel.push.repositories.LcpfEstablecimientoRepository;
import com.addcel.push.repositories.TUsuariosRepository;
import com.addcel.push.repositories.TokenEstablecimientoRepository;
import com.addcel.push.repositories.TokenUsuariosRepository;
import com.addcel.push.service.UtilsService;

@Service
public class UtilsServiceImpl implements UtilsService {
	
	@Autowired
	private TUsuariosRepository tusuariosRepo;
	@Autowired
	private LcpfEstablecimientoRepository lcpfEstableRepo;
	@Autowired
	private BitacoraPushRepository bitacoraRepo;
	@Autowired
	private TokenUsuariosRepository userTokenRepo;
	@Autowired
	private TokenEstablecimientoRepository estabTokenRepo;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilsServiceImpl.class);

	@Override
	public Boolean validaUsuario(Long id_usuario, String tipoUsuario) {
		LOGGER.debug("Validando al usuario "+id_usuario+"; tipo de usuario "+tipoUsuario);
		
		if (tipoUsuario.equals("USUARIO")) {
			return tusuariosRepo.existsById(id_usuario);
		}else {
			return lcpfEstableRepo.existsById(id_usuario);
		}
	}

	@Override
	public void guardarEnBitacora(Long idUsuario, String accion, String mensaje, String tipoUsuario) {
		try {
			LOGGER.debug("Insertando la accion en bitacora "+accion);
			BitacoraPush bitacora = new BitacoraPush();
			bitacora.setAccion(accion);
			bitacora.setFecha(new Date());
			bitacora.setHora(new Date());
			bitacora.setId_usuario(idUsuario);
			bitacora.setTipoUsuario(tipoUsuario);
			bitacora.setResultado(mensaje);
		 	BitacoraPush bit = bitacoraRepo.save(bitacora);
			LOGGER.debug("Agregado a la bitacora, id "+bit.getId_bitacora_push());
		} catch (Exception e) {
			LOGGER.error("Error al insertar en la bitacora "+e.getMessage());
		}
	}

	@Override
	public List<String> recuperaTokens(String tipoUsuario, Long idUsuario) throws Exception {
		List<String> tokens = null;
		try {
			if (tipoUsuario.equals("USUARIO")) {
				tokens = userTokenRepo.findTokensById(idUsuario);
				LOGGER.debug("Token recuperados del usuario "+tokens.size());
			}else {
				tokens = estabTokenRepo.findTokensById(idUsuario);
				LOGGER.debug("Token recuperados del comercio "+tokens.size());
			}
			return tokens;
		} catch (Exception e) {
			LOGGER.error("Error al consultar los tokens del usuario"+idUsuario+" en la BD");
			guardarEnBitacora(idUsuario, "Consulta de tokens en BD", "Error al consultar los tokens del usuario"+idUsuario+" en la BD", tipoUsuario);
			throw new Exception("Error al consultar los tokens del usuario"+idUsuario+" en la BD");
		}
	}

}
