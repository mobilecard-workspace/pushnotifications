package com.addcel.push.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.addcel.push.entities.dto.request.MassivePushRequest;
import com.addcel.push.entities.dto.response.MassivePushResponse;
import com.addcel.push.entities.dto.response.MessageError;
import com.addcel.push.repositories.BitacoraPushRepository;
import com.addcel.push.repositories.LcpfEstablecimientoRepository;
import com.addcel.push.repositories.TUsuariosRepository;
import com.addcel.push.service.MassivePushService;
import com.addcel.push.service.UtilsService;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.SendResponse;

@Service
public class MassivePushServiceImpl implements MassivePushService {
	
	@Autowired
	BitacoraPushRepository bitacoraRepo;
	@Autowired
	private TUsuariosRepository tusuariosRepo;
	@Autowired
	private LcpfEstablecimientoRepository lcpfEstableRepo;
	@Autowired
	private UtilsService utilsService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MassivePushServiceImpl.class);

	@Override
	public MassivePushResponse massivePush(MassivePushRequest request) throws Exception {
		MassivePushResponse response = null;
		List<MessageError> errors = null;
		int noToken = 0;
		int tokenVencido = 0;
		try {
			LOGGER.info("Iniciando envío de push masiva, request: {}",request);
			//agregar la validacion de si es por país o por lista de usuarios
			//si el array de usuario viene vacio, se le enviará a todos los que pertencen al pais requerido
			int numUsuarios = request.getUsuarios()!=null?request.getUsuarios().size():0;
			if (numUsuarios>0) {
				LOGGER.debug("Se tienen los ids de usuario a los que se quieren enviar: {}",numUsuarios);
				errors = sendMessageWithTokens(request);
				for (MessageError messageError : errors) {
					if (messageError.getError().contains("no tiene tokens registrados")) {
						noToken++;
					} else {
						tokenVencido++;
					}
				}
				response = new MassivePushResponse(0, "Notificaciones push enviadas con exito", tokenVencido,noToken, errors);
			} else {
				LOGGER.debug("No se tienen los ids de los usuarios a lo que se les quiere mandar la notificacion");
				LOGGER.debug("Se les mandará a todos los usuario con idPais: {}",request.getIdPais());
				errors = sendPushPerPais(request);
				for (MessageError messageError : errors) {
					if (messageError.getError().contains("no tiene tokens registrados")) {
						noToken++;
					} else {
						tokenVencido++;
					}
				}
				response = new MassivePushResponse(0, "Notificaciones push enviadas con exito", tokenVencido,noToken, errors);
			}
		} catch (Exception e) {
			LOGGER.error("Error al enviar la push masiva "+e.getMessage());
			utilsService.guardarEnBitacora(0L, "Envío de push masivas", "Error: "+e.getMessage(), "");
			throw new Exception("Error al enviar la push masiva "+e.getMessage());
		}
		return response;
	}
	
	private List<MessageError> sendMessageWithTokens(MassivePushRequest request) throws Exception {
		List<MessageError> response = null;
		try {
			LOGGER.debug("Enviando push a lista de usuarios: {}",request);
			List<Long> usuarios = request.getUsuarios();
			response = manageUsers(usuarios, request.getTipoUsuario(), request.getTitulo(), request.getMensaje(), request.getIdPais());
		} catch (Exception e) {
			LOGGER.error("Excepcion al enviar las push por usuarios: {}", e.getMessage());
			throw new Exception("Excepcion al enviar las push por usuarios"+e.getMessage());
		}
		return response;
	}
	
	private List<MessageError> sendPushPerPais(MassivePushRequest request) throws Exception{
		List<MessageError> response = new ArrayList<MessageError>();
		List<Long> usuariosFrom = null;
		try {
			LOGGER.debug("Enviando la push a todos los usuarios con el idPais: {}",request.getIdPais());
			if(request.getTipoUsuario().equals("USUARIO"))
				usuariosFrom = tusuariosRepo.findUserPerPais(request.getIdPais());
			else
				usuariosFrom = lcpfEstableRepo.findEstabPerPais(request.getIdPais().toString());
			
			
			if (usuariosFrom.size() > 0) {
				response.addAll(manageUsers(usuariosFrom, request.getTipoUsuario(), request.getTitulo(), request.getMensaje(), request.getIdPais()));
			} else {
				LOGGER.error("Error al recuperar los usuarios o establecimientos con idPais "+request.getIdPais());
				throw new Exception("Error al recuperar los usuarios o establecimientos con idPais "+request.getIdPais());
			}
		} catch (Exception e) {
			LOGGER.error("Excepcion al enviar las push por usuarios: {}", e.getMessage());
			throw new Exception("Excepcion al enviar las push al pais con id "+request.getIdPais()+", "+ e.getMessage());
		}
		return response;
	}
	
	private List<MessageError> manageUsers(List<Long> usuarios, String tipoUsuario, String titulo, String mensaje, Long idPais) throws Exception{
		List<MessageError> response = new ArrayList<MessageError>();
		try {
			LOGGER.debug("Gestionando la información de {} usuarios",usuarios.size());
			for (Long usuario : usuarios) {
				LOGGER.debug("Recuperando los token (s) del usuario con id: {}", usuario); 
				if (utilsService.validaUsuario(usuario, tipoUsuario)) {
					List<String> tokens = utilsService.recuperaTokens(tipoUsuario, usuario);
					if (tokens != null & tokens.size()>0) {
						List<MessageError> errors = sendPush(tokens, usuario, tipoUsuario, titulo, mensaje, idPais);
						for (MessageError e : errors) {
							response.add(e);
						}
					} else {
						response.add(new MessageError("El usuario "+usuario+" no tiene tokens registrados", usuario, idPais));
					}
				}else {
					response.add(new MessageError("El usuario "+usuario+" no existe en la base de datos", usuario, idPais));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error al recuperar los usuarios: {}", e.getMessage());
			throw new Exception("Error al recuperar los usuarios: "+e.getMessage());
		}
		return response;
	}
	
	private List<MessageError> sendPush(List<String> tokens, Long idUsuario, String tipoUsuario, String titulo, String mensaje, Long idPais){
		List<MessageError> response = new ArrayList<MessageError>();
		try {
			LOGGER.debug("El usuario con id {}, tiene {} tokens ",idUsuario, tokens.size());
			if (tokens.size() == 1) {
				MessageError error = new MessageError(sendIndividualPush(tokens.get(0), titulo, mensaje, tipoUsuario, idUsuario), idUsuario, idPais);
				response.add(error);
			} else {
				List<String> fireMessages = sendMultiplePushPerUser(tokens, titulo, mensaje, tipoUsuario, idUsuario);
				List<MessageError> aux = new ArrayList<MessageError>();
				fireMessages.forEach(m -> {
					MessageError error = new MessageError(m, idUsuario, idPais);
					aux.add(error);
				});
				response.addAll(aux);
			}
		} catch (Exception e) {
			LOGGER.error("Error al enviar las push: {}",e.getMessage());
			response.add(new MessageError("Error: "+e.getMessage()+" al enviar la push al usuario con id: "+idUsuario, idUsuario, idPais));
		}
		return response;
	}
	
	private String sendIndividualPush(String token, String titulo, String mensaje, String tipoUsuario, Long idUsuario) {
		try {
			LOGGER.debug("Construyendo la notificacion con el titulo: {}", titulo);
			//Construir el mensaje
			Message message = Message.builder()
					.setNotification(new Notification(titulo, mensaje))
				    .setToken(token)
				    .build();
			
				String responseFirebase = FirebaseMessaging.getInstance().send(message);
				// La respuesta es un string id que incluye el id del mensaje y de la Bd de firebase
				LOGGER.debug("Mensaje Enviado Exitosamente "+responseFirebase);
				return responseFirebase;
		    
		} catch (FirebaseMessagingException e) {
			LOGGER.error(e.getErrorCode());
			LOGGER.error("Error: "+ e.getMessage()+" Para el token "+token);
			return "Error "+e.getMessage()+" con el token del usuario "+idUsuario;
		}
	}
	
	private List<String> sendMultiplePushPerUser(List<String> tokens, String titulo, String mensaje, String tipoUsuario, Long idUsuario){
		List<String> response = new ArrayList<String>();
		try {
			LOGGER.debug("Construyendo notificacion con titulo: {}", titulo);
			//Se construye el mensaje, con la especificación para varios dispositivos
			MulticastMessage message = MulticastMessage.builder()
					.setNotification(new Notification(titulo, mensaje))
				    .addAllTokens(tokens)
				    .build();
			LOGGER.debug("Enviando la notificacion");
			
			//Este tipo de mensaje responde un objeto, en el cual vienen los tokens que fallaron
			BatchResponse firebaseResponse = FirebaseMessaging.getInstance().sendMulticast(message);
			//se evalua el conteo de errores
			if (firebaseResponse.getFailureCount() > 0) {
				List<SendResponse> responses = firebaseResponse.getResponses();
				List<String> failedTokens = new ArrayList<String>();
				List<String> goodTokens = new ArrayList<String>();
				responses.forEach(t -> {
					if (!t.isSuccessful()) {
						failedTokens.add(t.getException().getMessage());
					}else {
						goodTokens.add(t.getMessageId());
					}
				});
				
				LOGGER.debug("Tokens que fallaron: {}", failedTokens.size());
				LOGGER.debug("Tokens exitosos: {}", goodTokens.size());
			}
			LOGGER.debug("Notificacion enviada a todos los dispositivos");
		} catch (FirebaseMessagingException e) {
			LOGGER.debug("Error al enviar la notificacion "+e.getMessage());
			response.add("Error al enviar la notificacion a varios tokens"+e.getMessage());
		}
		return response;
	}
	
}
