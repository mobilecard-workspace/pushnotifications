 package com.addcel.push.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import com.addcel.push.entities.LcpfEstablecimiento;
import com.addcel.push.entities.NotificacionesPush;
import com.addcel.push.entities.ResponseException;
import com.addcel.push.entities.TokenEstablecimiento;
import com.addcel.push.entities.TokenUsuarios;
import com.addcel.push.entities.Usuario;
import com.addcel.push.entities.dto.MensajeNotificacion;
import com.addcel.push.entities.dto.request.NotificationParams;
import com.addcel.push.entities.dto.request.RequestDeleteToken;
import com.addcel.push.entities.dto.request.RequestInsertarToken;
import com.addcel.push.entities.dto.request.RequestNotificacion;
import com.addcel.push.entities.dto.response.ResponseEnviarNotificacion;
import com.addcel.push.entities.dto.response.ResponseInsertarToken;
import com.addcel.push.repositories.LcpfEstablecimientoRepository;
import com.addcel.push.repositories.NotificacionesPushRepository;
import com.addcel.push.repositories.TUsuariosRepository;
import com.addcel.push.repositories.TokenEstablecimientoRepository;
import com.addcel.push.repositories.TokenUsuariosRepository;
import com.addcel.push.service.PushMessageService;
import com.addcel.push.service.UtilsService;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.SendResponse;

@Service
public class PushMessageServiceImpl implements PushMessageService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PushMessageServiceImpl.class);
	
	@Autowired
	private UtilsService utilsService;
	@Autowired
	private NotificacionesPushRepository notificationRepo;
	@Autowired
	private TUsuariosRepository tusuariosRepo;
	@Autowired
	private LcpfEstablecimientoRepository lcpfEstableRepo;
	@Autowired
	private TokenUsuariosRepository userTokenRepo;
	@Autowired
	private TokenEstablecimientoRepository estabTokenRepo;
	

	@Override
	public ResponseEnviarNotificacion sendNotification(RequestNotificacion request) throws Exception {
		List<String> tokens = null;
		LOGGER.info("Se inicializa el envio del mensaje");
		try {
			if (utilsService.validaUsuario(request.getId_usuario(), request.getTipoUsuario())) {
				//Recuperar el mensaje de notificacion de la BD de mobile
				MensajeNotificacion notificacion = buscarnotificacion(request.getId_usuario(), request.getModulo(), request.getIdioma(), request.getParams() );
				tokens = utilsService.recuperaTokens(request.getTipoUsuario(), request.getId_usuario());	
					//Valida el número de tokens registrados
					if (tokens.size() == 1) {
						LOGGER.info("Enviar notificacion a un solo dispositivo");
						String notificationResponse = sendIndividualMessage(tokens.get(0), notificacion, request.getTipoUsuario(), request.getId_usuario());
						utilsService.guardarEnBitacora(request.getId_usuario(), "Envio de notificacion individual al usuario "+request.getId_usuario(), "Enviado correctamente", request.getTipoUsuario());
						return new ResponseEnviarNotificacion(0, "Notificacion enviada con exito al usuario", notificationResponse, new Date().getTime());
					}else {
						LOGGER.info("Enviar notificacion a varios dispositivos");
						int exitosos = sendMultiMessage(tokens, notificacion, request.getTipoUsuario(), request.getId_usuario());
						String resultado;
						if (exitosos>0) {
							resultado = "Notificacion enviada con exito a "+exitosos+" dispositivos";
						}else {
							throw new Exception("No se envió ninguna notificacion, los tokens del usuario no son válidos");
						}
						
						utilsService.guardarEnBitacora(request.getId_usuario(), "Envio de notificacion a varios dispositivos del usuario "+request.getId_usuario(), "Enviado correctamente", request.getTipoUsuario());
						return new ResponseEnviarNotificacion(0, "Notificacion enviada exitosamente a los dispositivos del usuario", resultado, new Date().getTime());
					}
			}else {
				LOGGER.error("Id de usuario inexistente en la BD: "+request.getId_usuario());
				throw new Exception("Se requiere ingresar un id de usuario o de establecimiento registrado en la BD");
			}
			
		} catch (Exception e) {
			utilsService.guardarEnBitacora(request.getId_usuario(), "Envio de notificacion al usuario "+request.getId_usuario(), "Error al enviar la notificacion "+e.getMessage(), request.getTipoUsuario());
			throw new Exception("Error al enviar la notificacion "+e.getMessage());
		}
	}
	
	@Override
	public ResponseInsertarToken insertarTokenUser(RequestInsertarToken request) throws Exception {
		ResponseInsertarToken response = null;
		LOGGER.info("Iniciando guardado de nuevo token para el usuario "+request.getId_usuario());
		try {
			if (utilsService.validaUsuario(request.getId_usuario(), request.getTipoUsuario()) && validaToken(request.getToken(), request.getId_usuario(), request.getTipoUsuario())) {
				if (request.getTipoUsuario().equals("USUARIO")) {
					Usuario user = tusuariosRepo.findById(request.getId_usuario()).orElse(null);
					LOGGER.info("Tipo de usuario "+request.getTipoUsuario());
					TokenUsuarios tokenUser = new TokenUsuarios();
					tokenUser.setUsuario(user);
					tokenUser.setUserToken(request.getToken());
					TokenUsuarios idToken = userTokenRepo.save(tokenUser);
					LOGGER.info("Token agregado exitosamente");
					response = new ResponseInsertarToken(0, "Token agregado exitosamente", idToken.getId_token(), new Date().getTime());
				}else {
					LOGGER.info("Tipo de usuario "+request.getTipoUsuario());
					TokenEstablecimiento tokenEstab = new TokenEstablecimiento();
					LcpfEstablecimiento comercio = lcpfEstableRepo.findById(request.getId_usuario()).orElse(null);
					tokenEstab.setComercio(comercio);
					tokenEstab.setUserToken(request.getToken());
					TokenEstablecimiento idToken = estabTokenRepo.save(tokenEstab);
					LOGGER.info("Token agregado exitosamente");
					response = new ResponseInsertarToken(0, "Token agregado exitosamente", idToken.getId_token(), new Date().getTime());
				}
			}else {
				LOGGER.error("Id de usuario inexistente en la BD: "+request.getId_usuario());
				throw new Exception("Se requiere ingresar un id de usuario o de establecimiento registrado en la BD");
			}
			utilsService.guardarEnBitacora(request.getId_usuario(), "Agregar token al usuario "+request.getId_usuario(), "Agregado correctamente", request.getTipoUsuario());
			
		} catch (DataIntegrityViolationException e) {
			LOGGER.error("Error al guardar el Token en BD "+e.getMessage());
			utilsService.guardarEnBitacora(request.getId_usuario(), "Agregar token al usuario "+request.getId_usuario(), "Error al guardar el token en BD "+e.getMessage(), request.getTipoUsuario());
			throw new Exception("Error al guardar el Token en BD "+e.getMessage());
		}
		return response;
	}
	
	@Override
	public ResponseException borrarToken(RequestDeleteToken request) throws Exception {
		ResponseException response = null;
		try {
			LOGGER.debug("Borrando token para el email: {}",request.getEmail());
			Long comercio = lcpfEstableRepo.findByComercio(request.getEmail());
			Long usuario = tusuariosRepo.findByEmail(request.getEmail());
			if (usuario != null) {
				List<String> userTokens = userTokenRepo.findTokensById(usuario);
				for (String token : userTokens) {
					LOGGER.debug("Borrando token para usuario");
					TokenUsuarios tokenUser = userTokenRepo.findByUserTokenAndUsuario_Id(token, usuario);
					userTokenRepo.deleteById(tokenUser.getId_token());
				}
			} else {
				LOGGER.error("No hay usuarios con ese correo en la Base de Datos");
			}
			
			if (comercio != null) {
				List<String> comercioTokens = estabTokenRepo.findTokensById(comercio);
				for (String token : comercioTokens) {
					LOGGER.debug("Borrando token para establecimiento");
					TokenEstablecimiento tokenEst = estabTokenRepo.findByUserTokenAndComercio_Id(token, comercio);
					estabTokenRepo.deleteById(tokenEst.getId_token());
				}
			} else {
				LOGGER.error("No hay un comercio registrado con ese correo en la BD");
			}
			
			LOGGER.debug("Tokens eliminados");
			response = new ResponseException(0, "Tokens Eliminados");
		} catch (Exception e) {
			LOGGER.error("Error al borrar el Token de la BD "+e.getMessage());
			utilsService.guardarEnBitacora(0L, "Borrado de Token para el usuario con email: "+request.getEmail(), "Error al borrar el token para el usuario: "+request.getEmail(), "Usuario");
			throw new Exception("Error al borrar el Token de la BD "+e.getMessage());
		}
		return response;
	}
	
	private String sendIndividualMessage(String token, MensajeNotificacion notificacion, String tipoUsuario, Long idUsuario) throws Exception {
		try {
			LOGGER.info("Construyendo la notificacion");
			//Construir el mensaje
			Message message = Message.builder()
					.setNotification(new Notification(notificacion.getTitulo(), notificacion.getMensaje()))
				    .setToken(token)
				    .build();
			
				String responseFirebase = FirebaseMessaging.getInstance().send(message);
				// La respuesta es un strin id que incluye el id del mensaje y de la Bd de firebase
				LOGGER.info("Mensaje Enviado Exitosamente "+responseFirebase);
				return responseFirebase;
		    
		} catch (FirebaseMessagingException e) {
			LOGGER.error(e.getErrorCode());
			LOGGER.error("Error: "+ e.getMessage()+" Para el token "+token);
			if (e.getMessage().contains("Requested entity was not found.")) {
				//en caso de error, se valida que no sea un token caducado; si lo es, se elimina de la BD de mobile
				if (tipoUsuario.equals("USUARIO")) {
					TokenUsuarios tokenUser = userTokenRepo.findByUserTokenAndUsuario_Id(token, idUsuario);
					userTokenRepo.deleteById(tokenUser.getId_token());
				}else {
					TokenEstablecimiento tokenEst = estabTokenRepo.findByUserTokenAndComercio_Id(token, idUsuario);
					estabTokenRepo.deleteById(tokenEst.getId_token());
				}
				
				LOGGER.debug("Se eliminó el token: "+token);
			}
			
			throw new Exception("Error: "+ e.getMessage()+" Para el token "+token);
		}
	}
	
	private int sendMultiMessage(List<String> registrationTokens, MensajeNotificacion notificacion, String tipoUsuario, Long idUsuario) throws Exception{
		try {
			LOGGER.info("Construyendo notificacion");
			//Se construye el mensaje, con la especificación para varios dispositivos
			MulticastMessage message = MulticastMessage.builder()
					.setNotification(new Notification(notificacion.getTitulo(), notificacion.getMensaje()))
				    .addAllTokens(registrationTokens)
				    .build();
			LOGGER.info("Enviando la notificacion");
			//Este tipo de mensaje responde un objeto, en el cual vienen los tokens que fallaron
			BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(message);
			//se evalua el conteo de errores
			if (response.getFailureCount() > 0) {
				List<SendResponse> responses = response.getResponses();
				List<String> failedTokens = new ArrayList<String>();
				for (int i = 0; i < responses.size(); i++) {
					//Verificamos los token que fallaron y los agregamos a una lista
					if (!responses.get(i).isSuccessful()) {
						failedTokens.add(registrationTokens.get(i));
					}
				}
				
				LOGGER.info("Tokens por eliminar: "+ failedTokens);
				
				//Finalmente, se eliminan los token que fallaron
				for (String failToken : failedTokens) {
					if (tipoUsuario.equals("USUARIO")) {
						TokenUsuarios tokenUser = userTokenRepo.findByUserTokenAndUsuario_Id(failToken, idUsuario);
						userTokenRepo.deleteById(tokenUser.getId_token());
					}else {
						TokenEstablecimiento tokenEst = estabTokenRepo.findByUserTokenAndComercio_Id(failToken, idUsuario);
						estabTokenRepo.deleteById(tokenEst.getId_token());
					}
					
				}
			}
			LOGGER.info("Notificacion enviada a todos los dispositivos");
			return response.getSuccessCount();
		} catch (FirebaseMessagingException e) {
			LOGGER.error("Error al enviar la notificacion "+e.getMessage());
			throw new Exception("Error al enviar la notificacion "+e.getMessage());
		}
	}
	
	private MensajeNotificacion buscarnotificacion(long idUsuario, Long modulo, String idioma, List<NotificationParams> params) throws Exception {
		LOGGER.info("Buscando la notificacion para el modulo "+modulo);
		
		try {
			NotificacionesPush push = notificationRepo.findById(modulo).orElse(null);
			if (push != null) {
				LOGGER.info("Notificacion por enviar: "+push.getTitulo());
				
				MensajeNotificacion notificacion = new MensajeNotificacion(push.getTitulo(), push.getMensaje());
				
				params.forEach(o -> {
					notificacion.setMensaje(notificacion.getMensaje().replace(o.getName(), o.getValue()));
				});
				
				LOGGER.info(notificacion.getMensaje());
				return notificacion;
			} else {
				throw new Exception("No existe una notificacion con el id "+modulo);
			}
		} catch (DataIntegrityViolationException e) {
			LOGGER.error("Error al buscar la notificacion en la BD "+e.getMessage());
			utilsService.guardarEnBitacora(idUsuario, "Buscar notificacion en BD", "Error al buscar la notificacion en la BD "+e.getMessage(), "");
			throw new Exception(e.getMessage());
		}
	}
	
	private boolean validaToken(String token, long idUsuario, String tipoUsuario) throws Exception{
		boolean tokenExistente = true;
		LOGGER.info("Validando que el token no esté registrado para el usuario "+idUsuario);
		try {
			if (tipoUsuario.equals("USUARIO")) {
				TokenUsuarios tokenUser = userTokenRepo.findByUserTokenAndUsuario_Id(token, idUsuario);
				if (tokenUser != null)
					throw new Exception("El usuario ya tiene registrado ese token en la BD");
			} else {
				TokenEstablecimiento tokenEstab = estabTokenRepo.findByUserTokenAndComercio_Id(token, idUsuario);
				if (tokenEstab != null)
					throw new Exception("El establecimiento ya tiene registrado ese token en la BD");
			}
		} catch (DataIntegrityViolationException e) {
			LOGGER.error("Error al buscar el token en la BD "+e.getMessage());
			utilsService.guardarEnBitacora(idUsuario, "Buscar token de "+tipoUsuario+" en BD", "Error al buscar el token de "+tipoUsuario+" en la BD "+e.getMessage(), "");
			throw new Exception("Error al buscar el token en la BD "+e.getMessage());
		}
		return tokenExistente;
	}

}
