package com.addcel.push.service;

import java.util.List;

public interface UtilsService {

	Boolean validaUsuario(Long id_usuario, String tipoUsuario);
	
	void guardarEnBitacora(Long idUsuario, String accion, String mensaje, String tipoUsuario);
	
	List<String> recuperaTokens(String tipoUsuario, Long idUsuario) throws Exception;
}
