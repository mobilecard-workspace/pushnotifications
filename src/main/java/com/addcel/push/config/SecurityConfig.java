package com.addcel.push.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Value("${firebase.config.securityname}")
	private String name;
	@Value("${firebase.config.securitypass}")
	private String pass;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
//		http.csrf().disable()
//			.authorizeRequests().anyRequest().authenticated()
//			.and()
//			.httpBasic();
		
		http.cors()
    	.and().csrf().disable()
    	.authorizeRequests()
    	.anyRequest().permitAll()
		.and()
		.httpBasic();
	}
	
	@Autowired
	protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
		auth.inMemoryAuthentication()
			.withUser(name)
			.password("{noop}"+pass)
			.roles("USER");
	}
}
