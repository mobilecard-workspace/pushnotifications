package com.addcel.push.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.addcel.push.entities.BitacoraPush;

@Repository
public interface BitacoraPushRepository extends JpaRepository<BitacoraPush, Long> {

}
