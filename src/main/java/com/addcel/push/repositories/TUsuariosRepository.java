package com.addcel.push.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.addcel.push.entities.Usuario;

@Repository
public interface TUsuariosRepository extends JpaRepository<Usuario, Long> {

	@Query("SELECT u.id FROM Usuario u WHERE u.idPais = ?1")
	List<Long> findUserPerPais(Long idPais);
	
	@Query("SELECT u.id FROM Usuario u WHERE u.email = ?1")
	Long findByEmail(String email);
}
