package com.addcel.push.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.addcel.push.entities.TokenUsuarios;

@Repository
public interface TokenUsuariosRepository extends JpaRepository<TokenUsuarios, Long> {

	@Query("SELECT t.userToken FROM TokenUsuarios t WHERE t.usuario.id = ?1")
	List<String> findTokensById(Long id_usuario);
	
	TokenUsuarios findByUserTokenAndUsuario_Id(String userToken, Long idUsuario);
	
	@Transactional
	Long deleteById(long idToken);
}
