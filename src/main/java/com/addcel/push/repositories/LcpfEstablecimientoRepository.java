package com.addcel.push.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.addcel.push.entities.LcpfEstablecimiento;

@Repository
public interface LcpfEstablecimientoRepository extends JpaRepository<LcpfEstablecimiento, Long> {

	@Query("SELECT e.id FROM LcpfEstablecimiento e WHERE e.idPais = ?1")
	List<Long> findEstabPerPais(String idPais);
	
	@Query("SELECT e.id FROM LcpfEstablecimiento e WHERE e.usuario = ?1")
	Long findByComercio(String email);
}
