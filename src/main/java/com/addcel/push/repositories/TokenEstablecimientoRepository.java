package com.addcel.push.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.addcel.push.entities.TokenEstablecimiento;

@Repository
public interface TokenEstablecimientoRepository extends JpaRepository<TokenEstablecimiento, Long> {

	@Query("SELECT t.userToken FROM TokenEstablecimiento t WHERE t.comercio.id = ?1")
	List<String> findTokensById(long id_establecimiento);
	
	TokenEstablecimiento findByUserTokenAndComercio_Id(String tokenEstablecimiento, long idEstablecimiento);
	
	@Transactional
	Long deleteById(long idToken);
}
