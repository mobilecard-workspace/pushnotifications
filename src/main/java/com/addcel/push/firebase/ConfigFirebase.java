package com.addcel.push.firebase;

import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseOptions;

@Component
public class ConfigFirebase implements ApplicationRunner{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigFirebase.class);
	
	@Value("${firebase.config.credentialdir}")
	private String FILEDIR;
	@Value("${firebase.config.url}")
	private String DATABASE_URL;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		LOGGER.info("init Firebase App");
			try {
				FirebaseOptions options;
				//File serviceAccountFile = ResourceUtils.getFile(FILEDIR);
				InputStream serviceAccount = new ClassPathResource(FILEDIR).getInputStream();
				//FileInputStream serviceAccount = new FileInputStream(serviceAccountFile);
				options = new FirebaseOptions.Builder()
					    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
					    .setDatabaseUrl(DATABASE_URL)
					    .build();
				FirebaseApp.initializeApp(options);
			LOGGER.info("Firebase App initializer");
		} catch (Exception e) {
			LOGGER.error("Error initializing Firebase App "+e.getMessage());
			throw new FirebaseException(e.getMessage());
		}
	}

}
