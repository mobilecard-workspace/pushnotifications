package com.addcel.push.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="LCPF_establecimiento")
public class LcpfEstablecimiento implements Serializable {

	private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name="usuario")
    private String usuario;
    
    @Column(name="pass") 
    private String pass;    
    
    @Column(name="nombre_establecimiento") 
    private String nombreEstablecimiento;
    
    @Column(name="rfc") 
    private String rfc;
    
    @Column(name="razon_social") 
    private String razonSocial;
    
    @Column(name="representante_legal") 
    private String representanteLegal;
    
    @Column(name="direccion_establecimiento") 
    private String direccionEstablecimiento;
    
    @Column(name="email_contacto") 
    private String emailContacto;
    
    @Column(name="telefono_contacto")
    private String telefonoContacto;
    
    @Column(name="id_banco") 
    private String idBanco;
    
    @Column(name="cuenta_clabe") 
    private String cuentaClabe;
    
    @Column(name="nombre_prop_cuenta") 
    private String nombrePropCuenta;
    
    @Column(name="estatus") 
    private Long status;
    
    @Column(name="id_account") 
    private Long idAccount;
    
    @Column(name="comision_fija")
    private BigDecimal comisionFija;
    
    @Column(name="comision_porcentaje") 
    private BigDecimal comisionPorcentaje;
    
    @Column(name="urlLogo") 
    private String urlLogo;
    
    @Column(name="cuenta_tipo") 
    private String cuentaTipo;
    
    @Column(name="codigo_banco")
    private String codigoBanco;
    
    @Column(name="id_aplicacion") 
    private Long idAplicacion;
    
    @Column(name="tipo_comision") 
    private Long tipoComision;
    
    @Column(name="img_ine") 
    private String  imgIne;
    
    @Column(name="img_domicilio")
    private String imgDomicilio;
    
    @Column(name="fecha_registro")
    private String fecha;
    
    @Column(name="representante_nombre") 
    private String representanteNombre;
    
    @Column(name="representante_paterno")
    private String representantePaterno;
    
    @Column(name="representante_materno")
    private String representanteMaterno;
    
    @Column(name="representante_curp") 
    private String representanteCurp;
    
    @Column(name="calle") 
    private String calle;
    
    @Column(name="colonia") 
    private String colonia;
   
    @Column(name="cp")
    private String cp;
    
    @Column(name="municipio") 
    private String municipio;
    
    @Column(name="id_estado") 
    private Long idEstado;
    
    @Column(name="sms_code") 
    private String smsCode;
    
    @Column(name="tipo_documento")
    private String tipoDocumento;
    
    @Column(name="numero_documento") 
    private String numeroDocumento;
    
    @Column(name="fecha_nacimiento") 
    private String fechaNacimiento;
    
    @Column(name="nacionalidad")
    private String nacionalidad;
    
    @Column(name="distrito") 
    private String distrito;
    
    @Column(name="provincia")
    private String provincia;
    
    @Column(name="departamento") 
    private String departamento;
    
    @Column(name="centro_laboral") 
    private String centroLaboral;
    
    @Column(name="ocupacion") 
    private String ocupacion;
    
    @Column(name="cargo") 
    private String cargo;
    
    @Column(name="institucion") 
    private String institucion;
    
    @Column(name="cci") 
    private String cci;
    
    @Column(name="id_pais") 
    private String idPais;
    
    @Column(name="digito_verificador") 
    private String digitoVerificador;
    
    @Column(name="jumio_status") 
    private Long jumioStatus;
    
    @Column(name="jumio_reference") 
    private String jumioReference;
    
    @OneToMany(mappedBy = "comercio")
	@JsonManagedReference
	private List<TokenEstablecimiento> tokens;
}
