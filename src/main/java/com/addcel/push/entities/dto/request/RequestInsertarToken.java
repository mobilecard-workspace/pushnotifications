package com.addcel.push.entities.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestInsertarToken {
	private long id_usuario;
	private String tipoUsuario;
	private String idioma;
	private long idPais;
	private long idApp;
	private String token;
}
