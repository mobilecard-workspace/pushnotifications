package com.addcel.push.entities.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseEnviarNotificacion {

	private int code;
	private String message;
	private String idPush;
	private long succesDate;
}
