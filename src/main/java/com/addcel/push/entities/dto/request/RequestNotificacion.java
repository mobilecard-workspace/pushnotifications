package com.addcel.push.entities.dto.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestNotificacion {

	private long modulo;
	private long id_usuario;
	private String tipoUsuario;
	private String idioma;
	private long idPais;
	private long idApp;
	private List<NotificationParams> params;
}
