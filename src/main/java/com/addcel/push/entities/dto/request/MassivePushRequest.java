package com.addcel.push.entities.dto.request;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MassivePushRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idPais;
	@NotNull(message = "Se debe especificar el tipo de usuario")
	private String tipoUsuario;
	@NotNull(message = "La notificacion debe contener un titulo")
	private String titulo;
	@NotNull(message = "La notificacion debe contener un mensaje")
	private String mensaje;
	private List<Long> usuarios;

}
