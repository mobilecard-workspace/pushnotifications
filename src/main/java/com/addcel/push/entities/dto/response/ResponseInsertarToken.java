package com.addcel.push.entities.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseInsertarToken {

	private int code;
	private String message;
	private long id_token;
	private long currentDate;
	
}
