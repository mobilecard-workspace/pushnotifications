package com.addcel.push.entities.dto.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageError implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String error;
	private Long idUsuario;
	private Long idPais;

}
