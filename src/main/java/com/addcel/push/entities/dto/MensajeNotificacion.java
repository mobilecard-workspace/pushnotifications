package com.addcel.push.entities.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MensajeNotificacion {

	private String titulo;
	private String mensaje;
}
