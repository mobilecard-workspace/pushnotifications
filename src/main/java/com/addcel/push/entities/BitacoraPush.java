package com.addcel.push.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="bitacora_push")
public class BitacoraPush implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_bitacora_push")
	private Long id_bitacora_push;
	
	@Temporal(TemporalType.DATE)
    @Column(name="bit_fecha")
    private Date fecha;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="bit_hora")
    private Date hora;
    
    @Column(name = "id_usuario")
    private Long id_usuario;
    
    @Column(name = "tipo_usuario")
    private String tipoUsuario;
    
    @Column(name = "resultado")
    private String resultado;
    
    @Column(name = "accion")
    private String accion;
}
