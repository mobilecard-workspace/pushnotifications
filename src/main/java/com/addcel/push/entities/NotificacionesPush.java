package com.addcel.push.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="notificaciones_push")
public class NotificacionesPush implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_notificacion_push")
	private Long id_notificacion_push;
	
	@Column(name = "modulo")
	private String modulo;
	
	@Column(name = "titulo")
	private String titulo;
	
	@Column(name = "mensaje")
	private String mensaje;
	
	@Column(name = "idioma")
	private String idioma;

}
